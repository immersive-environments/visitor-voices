# Heimaten backend and user Q&A interface

# How to install using docker
1) Make sure you have installed docker in the machine
2) git clone this repo
3) ``` docker-compose up --detach```


## How to update using docker
1) backup backendsettings.yml
2)
```
git fetch origin
git reset --hard origin/develop
```

3)
```docker-compose up --build --force-recreate```

4)
```
docker-compose up -d
```

5)
```cd backend/data
git checkout f831fb62facf28eb133a39cb24cb4442f7aebc82 -- questions_en.yml
```


## How to run it
### Frontend
``` sh
npm run dev
```

Using the Dockerfile
in ./frontend
``` sh
docker build -t museum-frontend:0.1 .
docker run -it -p 8080:80 --rm --name museum-front museum-frontend:0.1
```

### Backend

#### Installing
``` sh
pipenv install
```

#### Running Server
``` sh
pipenv run uvicorn museum.main:app --reload
```

##### Experiments in Jupyter notebook
```
pipenv run jupyter notebook
```
