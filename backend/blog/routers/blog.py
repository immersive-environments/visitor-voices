from typing import List
from fastapi import APIRouter, Depends, Response, HTTPException, status
from .. import schemas, models
from ..database import get_db
from sqlalchemy.orm import Session
from ..hashing import Hash

router = APIRouter(
  prefix='/blog',
  tags=['blog']
)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: schemas.Blog, db: Session = Depends(get_db)):
  """
  llalala
  """
  new_blog = models.Blog(title=request.title, body=request.body, user_id=2)
  db.add(new_blog)
  db.commit()
  db.refresh(new_blog)
  return new_blog

@router.get('/', response_model=List[schemas.ShowBlog])
def all(db: Session = Depends(get_db)):
  blogs = db.query(models.Blog).all()
  return blogs

@router.get('/{id}', status_code=status.HTTP_200_OK)
def show(id, response: Response, db: Session = Depends(get_db)):
  blog = db.query(models.Blog).filter(models.Blog.id == id).first()

  if not blog:
    raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                        detail= f'Blog with the id {id} is not available')
  return blog

@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete(id, db: Session = Depends(get_db)):
  blog = db.query(models.Blog).filter(models.Blog.id == id)

  if not blog.first():
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Blog with the id {id} is not available')

  blog.delete(synchronize_session=False)
  db.commit()
  return {'detail': f'{id} is deleted'}

@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED, response_model=schemas.ShowBlog)
def update(id, request: schemas.Blog, db: Session = Depends(get_db)):
  blog = db.query(models.Blog).filter(models.Blog.id == id)

  if not blog.first():
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Blog {id} does not exist')

  blog.update(request, synchronize_session=False)
  db.commit()

  return blog.first()
