from fastapi import FastAPI
from . import models
from .database import engine, getSettingsLocal
from .routers import admin, question, answer
from fastapi.middleware.cors import CORSMiddleware
import logging

app = FastAPI()

log = logging.getLogger("uvicorn")
log.setLevel(logging.DEBUG)

settings = getSettingsLocal()
print(settings['CORS'])

origins = settings['CORS']

app.add_middleware(
  CORSMiddleware,
  allow_origins=origins,
  allow_credentials=True,
  allow_methods=["*"],
  allow_headers=["*"],
)

models.Base.metadata.create_all(engine)

app.include_router(admin.router)
app.include_router(question.router)
app.include_router(answer.router)
