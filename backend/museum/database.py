from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import yaml

SQLALCHEMY_DATABASE_URL = 'sqlite:///./data/museum.db'

engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False})

SessionLocal = sessionmaker(bind=engine, autocommit=False, autoflush=False)

Base = declarative_base()


with open("./data/questions_en.yml", 'r') as stream:
  try:
    questions_en = yaml.safe_load(stream)
  except yaml.YAMLError as e:
    print(e)

with open("./data/questions_de.yml", 'r') as stream:
  try:
    questions_de = yaml.safe_load(stream)
  except yaml.YAMLError as e:
    print(e)


questions = {
  'en': questions_en,
  'de': questions_de
}

def get_questions():
  yield questions

def get_db():
  db = SessionLocal()
  try:
    yield db
  finally:
    db.close()

def getSettingsLocal():
  global settings

  with open("./data/settings.yml", 'r') as stream:
    try:
      settings = yaml.safe_load(stream)
    except yaml.YAMLError as e:
      print(e)
  return settings

def saveSettingsLocal(settings):
  with open("./data/settings.yml", 'w') as file:
    try:
      yaml.safe_dump(settings, file)
    except yaml.YAMLError as e:
      print(e)
  return getSettingsLocal()

getSettingsLocal()

def get_settings():
  yield settings