from typing import List, Optional
from fastapi import APIRouter, Depends, Response, HTTPException, status
from .. import schemas, models
from ..database import get_db, get_questions
from sqlalchemy.orm import Session
from sqlalchemy import asc, desc
from ..hashing import Hash

router = APIRouter(
  prefix='/question',
  tags=['Question']
)

"""
List all questions
"""
@router.get('/')
def all(db: Session = Depends(get_db), questions: object = Depends(get_questions)):
  
  return questions