from typing import List, Optional, Annotated
from fastapi import APIRouter, Depends, Response, HTTPException, status, File, Form, UploadFile
from .. import schemas, models
from ..database import get_db, get_questions, get_settings
from sqlalchemy.orm import Session
from sqlalchemy import asc, desc, inspect
from ..hashing import Hash
import cld3

import logging
log = logging.getLogger("uvicorn")


router = APIRouter(
  prefix='/answer',
  tags=['Answer']
)
"""
file: Annotated[UploadFile, File()],
audioUrl: Annotated[str, Form()],
text: Annotated[str, Form()], 
"""

#def create(request: schemas.BaseAnswer, db: Session = Depends(get_db)):
@router.post('/message', status_code=status.HTTP_201_CREATED)
def create(
          area_id: Annotated[int, Form()],
          text: Annotated[str, Form()],
          message: Annotated[UploadFile, File()],
          message_duration: Annotated[str, Form()] = '',
          score: Annotated[int, Form()] = 0,
          views: Annotated[int, Form()] = None,
          language_question: Annotated[str, Form()] = '',
          language_guessed_answer: Annotated[str, Form()] = '',
          choice_id: Annotated[int, Form()] = -1,
          subchoice_id: Annotated[int, Form()] = -1,
          user_name: Annotated[str, Form()] = '',
          user_place: Annotated[str, Form()] = '',
          db: Session = Depends(get_db)):
  """
  Create an answer
  """

  lan = cld3.get_language(text)
  if (lan.is_reliable):
    language_guessed_answer = lan.language
  else:
    language_guessed_answer = None

  name = message.filename.split('/')[-1]
  path = '/srv/voices/html/home/voices/' + name

  try:
    content = message.file.read()
    with open(path, 'wb') as f:
      f.write(content)
  except Exception as e:
      return {"message": "There was an error uploading the file", "error": e }
  finally:
    message.file.close()

  answer = models.Answer(
    text=text,
    score=score,
    language_question=language_question,
    language_guessed_answer=language_guessed_answer,
    area_id=area_id,
    choice_id=choice_id,
    subchoice_id=subchoice_id,
    user_name=user_name,
    user_place=user_place,
    message=name,
    message_duration=message_duration
  )

  db.add(answer)
  db.commit()
  db.refresh(answer)
  return answer

@router.get('/', response_model=List[schemas.AnswerShow])
def all(skip: int = 0, limit: int = 10, lang: Optional[str] = None, area: Optional[str] = 'all', order_by: Optional[str] = "desc_time", db: Session = Depends(get_db), questions: object = Depends(get_questions)):
  """
  List all answers
  """
  print("---------------------------------")
  answers = db.query(models.Answer)

  if lang and lang != 'all':
    answers = answers.filter(models.Answer.language_question == lang)

  if area and area != 'all':
    answers = answers.filter(models.Answer.area_id == area)

  #print(order_by)
  if order_by == 'desc_time':
    answers = answers.order_by(desc(models.Answer.time_created))
  elif order_by == 'asc_time':
    answers = answers.order_by(asc(models.Answer.time_created))
  elif order_by == 'desc_score':
    answers = answers.order_by(desc(models.Answer.score))
  elif order_by == 'asc_score':
    answers = answers.order_by(asc(models.Answer.score))

  allAnswers = answers.filter(models.Answer.score >= 0).offset(skip).limit(limit).all()

  for a in allAnswers:
    #print(a)
    addPlaceholder(questions, a)

  return allAnswers



minViewScore = 0
isShutdown = False

@router.get('/view/', status_code=status.HTTP_200_OK, response_model=schemas.AnswerShow)
def get(area: int, lang_question: Optional[str] = None, lang_answered: Optional[str] = None, db: Session = Depends(get_db), questions: object = Depends(get_questions), settings: object = Depends(get_settings)):
  """
  Get a pair answer-question acording to the number of reproductions
  """

  #print(settings['playrating'])

  # 1)
  # is there any answer with 0 views?
  answers = db.query(models.Answer)
  # print(area)
  answers = answers.filter(models.Answer.area_id == area)
  answer = answers.filter(models.Answer.views == 0).first()

  if answer is not None:
    # print('qq')
    answer.views = answer.views + 1
    db.commit()
    addPlaceholder(questions, answer)

    return answer


  # 2)
  # retrieve last shown
  answers = db.query(models.Answer)
  answers = answers.filter(models.Answer.area_id == area)

  # filter out those answers with negative score
  # && minViewScore that is set through the admin interface
  minViewScore = int(settings['playrating']) * 10
  #print(minViewScore)
  answers = answers.filter(models.Answer.score >= minViewScore )

  if lang_question:
    answers = answers.filter(models.Answer.language_question == lang_question)

  if lang_answered:
    answers = answers.filter(models.Answer.language_answered == lang_answered)

  answers = answers.order_by(asc(models.Answer.time_updated))

  if not answers.first():
    raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, detail= f'No answers founds')

  # post retrieval
  answer = answers.first()
  answer.views = answer.views + 1
  db.commit()
  addPlaceholder(questions, answer)
  answer.kill = isShutdown

  return answer



@router.get('/shutdown', status_code=status.HTTP_200_OK)
def shutdown():
  global isShutdown
  """
  Send signal to all computers to shutdown"
  """
  isShutdown = True
  return Response("shutdown: " + str(isShutdown))

def object_as_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}

def addPlaceholder(questions, answer):
  # return
  areas = questions[answer.language_question]['areas']

  selectedArea = [a for a in areas if a['id'] == answer.area_id][0]

  if 'choices' in selectedArea:
    selectedChoice = [c for c in selectedArea['choices'] if c['id'] == answer.choice_id][0]

    # this is basically for question 4
    if 'subchoices' in selectedChoice:
      selectedSubchoice = [c for c in selectedChoice['subchoices'] if c['id'] == answer.subchoice_id][0]
      placeholder = selectedSubchoice['placeholder']
    else:
      placeholder = selectedChoice['placeholder']

    # inject here the placeholder so it's easier to get
    answer.placeholder = placeholder

    #print(questions['en']['areas'][0]['choices'][0])

  if not 'choices' in selectedArea:
    answer.placeholder = selectedArea['placeholder']

  return answer

@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=schemas.AnswerShow)
def show(id, response: Response, db: Session = Depends(get_db), questions: object = Depends(get_questions)):
  """
  Show an answer by id
  """
  answer = db.query(models.Answer).filter(models.Answer.id == id).first()

  if not answer:
    raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                        detail= f'Answer with the id {id} is not available')

  addPlaceholder(questions, answer)

  return answer

@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete(id, db: Session = Depends(get_db)):
  """
  Delete an answer by id
  """
  answer = db.query(models.Answer).filter(models.Answer.id == id)

  if not answer.first():
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Answer with the id {id} is not available')

  answer.delete(synchronize_session=False)
  db.commit()
  return {'detail': f'answer {id} is deleted'}

@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED, response_model=schemas.AnswerShow)
def update(id, request: schemas.BaseAnswer, db: Session = Depends(get_db)):
  """
  Modify an answer by id
  """
  answer = db.query(models.Answer).filter(models.Answer.id == id)

  if not answer.first():
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Answer {id} does not exist')

  answer.update(request.__dict__, synchronize_session=False)
  db.commit()

  return answer.first()
