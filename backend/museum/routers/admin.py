import os
from datetime import datetime
from fastapi import APIRouter, Depends, Response, HTTPException, Request
from fastapi.responses import FileResponse
from pydantic import BaseModel
from ..database import get_settings, getSettingsLocal, saveSettingsLocal
import zipfile
import yaml

router = APIRouter(
  prefix='/admin',
  tags=['Admin']
)

BACKUP_FOLDER = './data/backups/'

@router.get('/download')
def download():
  """
  Download a database backup
  """
  #print(os.getcwd())

  # if no backup folder create it
  if not os.path.exists(BACKUP_FOLDER):
    os.makedirs(BACKUP_FOLDER)

  now = datetime.now()

  # get todays date
  filename = 'heimaten_database_' + now.strftime('%Y_%m_%d__%H_%M_%S') + '.zip'
  filepath = BACKUP_FOLDER + filename
  #print(filename)

  # copy database to backup folder appending the date to the name
  zipfile.ZipFile(filepath, mode='w').write('./data/museum.db')


  return FileResponse(path=filepath, filename=filename, media_type='application/zip')


class RatingModel(BaseModel):
  rating: int

@router.post('/playrating')
def set_playrating(payload: RatingModel):
  print(payload.rating)
  settings = getSettingsLocal()
  settings['playrating'] = payload.rating

  print('------')
  print(settings['playrating'])
  print('------')
  ratingsettings = saveSettingsLocal(settings)

  return settings

@router.get('/settings')
def get_settings():
  """
  Get settings
  """
  return getSettingsLocal()
 