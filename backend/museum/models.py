from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from .database import Base
from sqlalchemy.sql import func
  
class Session(Base):
  __tablename__ = 'sessions'
  id = Column(Integer, primary_key=True, index=True)

  time_created = Column(DateTime(timezone=True), server_default=func.now())
  time_updated = Column(DateTime(timezone=True), onupdate=func.now())

class Answer(Base):
  __tablename__ = 'answers'
  id = Column(Integer, primary_key=True, index=True)
  text = Column(String)
  score = Column(Integer)
  views = Column(Integer, default=0)
  language_question = Column(String)
  language_guessed_answer = Column(String)
  placeholder = Column(String, default='')
  area_id = Column(Integer)
  choice_id = Column(Integer)
  subchoice_id = Column(Integer)
  user_name = Column(String)
  user_place = Column(String) 
  message = Column(String, default='')
  message_duration = Column(String, default='00:00')

  time_created = Column(DateTime(timezone=True), server_default=func.now())
  time_updated = Column(DateTime(timezone=True), onupdate=func.now())
