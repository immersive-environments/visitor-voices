from typing import List, Optional, Annotated
from fastapi import File, UploadFile, Form
from pydantic import BaseModel

""" 
Answer Schema
"""
class BaseAnswer(BaseModel):
  text: str
  score: Optional[int] = 0
  views: Optional[int] = None
  language_question: Optional[str] = None
  language_guessed_answer: Optional[str] = None
  area_id: int
  choice_id: Optional[int] = -1
  subchoice_id: Optional[int] = -1
  user_name: Optional[str] = None
  user_place: Optional[str] = None
  message: Optional[str] = None
  message_duration: Optional[str] = None


class AnswerShow(BaseAnswer):
  id: int
  placeholder: str
  kill: Optional[bool] = False  
  
  class Config():
    orm_mode = True
