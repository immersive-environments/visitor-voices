#!/bin/bash

PYTHON_VERSION=3.9

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

pyenv local $PYTHON_VERSION
pipenv run uvicorn museum.main:app --host 0.0.0.0 --port 8001 --reload
