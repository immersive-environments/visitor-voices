from museum.database import SessionLocal, engine
from sqlalchemy.orm import Session
from museum import models
from museum.models import Question, Choice, Answer
import yaml
import pprint

pp = pprint.PrettyPrinter(indent=4)



with open("./data/questions_en.yml", 'r') as stream:
  try:
    questions = yaml.safe_load(stream)
  except yaml.YAMLError as e:
    print(e)

with open("./data/answers.yml", 'r') as stream:
  try:
    answers = yaml.safe_load(stream)
  except yaml.YAMLError as e:
    print(e)


# pp.pprint(questions)
# pp.pprint(answers)


models.Base.metadata.create_all(bind=engine)

db = SessionLocal()

for key in questions:
  area = key.split('_')[1]
  question = questions[key]

  pp.pprint(area)
  pp.pprint(questions[key])
  
  q = Question()
  q.area = int(area)
  q.text = question['text']
  q.desc = question['desc']
  q.answer_size = int(question['answer_size'])
  q.language = 'en'
  
  for choice in question['choices']:
    pp.pprint(choice)
    c = Choice(text=choice['text'], prompt=choice['prompt'])
    q.choices.append(c)

    try:
      #db.add(choice1)
      db.add(q)
      db.commit()
      print('added')
    except:
      db.rollback()
      print('not added')

questions = db.query(Question).all()
print(questions)


if False:
  c = db.query(Choice).filter(Choice.id == 1).first()
  print('>>')
  print(c.answers[0])
  print(c.answers[0].id)
  print('<<')
  pp.pprint(c.id)

  a = db.query(Answer).filter(Answer.id == 2).first()
  print(a.choice.id)
  print(a.choice.text)

#user1 = db.query(User).filter(User.email == 'qoiqoiq@oiqo.com').first()
#item1 = Item(title='potato')
#user1.items.append(item1)
#db.commit()

#for i in user1.items:
#  pp.pprint(i.__dict__)
#  print(i.owner.id)
