from database import SessionLocal, engine
from sqlalchemy.orm import Session
import models
from models import User, Item
import pprint

pp = pprint.PrettyPrinter(indent=4)

models.Base.metadata.create_all(bind=engine)

db = SessionLocal()

user = User()
user.email = 'qoiqoiq@oiqo.com'
user.is_active = True

user1 = User(email = 'qq@qq.qq', is_active=False)

try:
  db.add(user)
  db.commit()
  print('added')
except:
  db.rollback()
  print('not added')

try:
  db.add(user1)
  db.commit()
except:
  db.rollback()

users = db.query(User).all()
print(users)

user1 = db.query(User).filter(User.email == 'qoiqoiq@oiqo.com').first()
item1 = Item(title='potato')
user1.items.append(item1)
db.commit()

for i in user1.items:
  pp.pprint(i.__dict__)
  print(i.owner.id)
