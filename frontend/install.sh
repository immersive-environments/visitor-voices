#!/bin/bash

NODE_VERSION=18.19.0
INSTALL_PATH=/srv/voices/html

fail()
{
	echo $1
	exit 1
}

nvs use $NODE_VERSION

rm -rf dist dist_*

npm run build_admin && npm run build_home || fail "Build failed"

rm -rf $INSTALL_PATH/admin
cp -rv dist_admin $INSTALL_PATH/admin

rm -rf $INSTALL_PATH/home
cp -rv dist_home $INSTALL_PATH/home
ln -sf /srv/voices/messages $INSTALL_PATH/home/voices
