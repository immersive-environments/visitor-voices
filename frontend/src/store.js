// store.js
import { reactive } from 'vue'
import { v4 as uuidv4 } from 'uuid'
import axios from 'axios'

export const store = reactive({
  id: null,
  questions: null,
})

export const mutations = ({
  setRandomId() {
    store.id = uuidv4()
  },
  setQuestions(questions) {
    store.questions = questions
   // console.log('setting questions', store.questions)
  }
})

window.hostname = 'https://' + window.location.hostname + ':8000'

export const fetchQuestions = async () => {
  let query = `${window.hostname}/question/`
  // console.log(query)

  return axios
    .get(query)
    .then(response => {
      // console.log(response.data)
      mutations.setQuestions(response.data)
      // console.log('fetchQuestions 2')
    })
    .catch(e => { 
      this.questions = []
      console.log('error', e)
    })
}
