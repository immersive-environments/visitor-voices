import { createApp } from 'vue'
import { router } from './router'
import App from './App.vue'
import './index.css'
import { Buffer } from 'buffer'
import { createI18n } from 'vue-i18n' //'@intlify/vite-plugin-vue-i18n'

const messages = {
  en: { hello: 'hi there!' },
  ja: { hello: 'こんにちは！' }
}

const i18n = createI18n({
  global: true,
  locale: 'de',
  fallbackLocale: 'en',
  messages
})

globalThis.Buffer = Buffer

const app = createApp(App)
app.use(router)
app.use(i18n)
app.mount('#app')
app.config.globalProperties.window = window

/*
app.config.errorHandler = (err, vm, info) => {
  console.log('potato')
  console.log(err)
  console.log(info)
  router.push({ name: 'Error', params: { msg: 'hola', error: err } })
}
*/

i18n.global.locale = 'en'
