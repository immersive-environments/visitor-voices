import { createRouter, createWebHashHistory } from 'vue-router'
import { store, mutations, fetchQuestions } from './store.js'
// import HelloWorld from '#components/HelloWorld'

export const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { //removeadmin
      path: '', //removeadmin
      redirect: '/admin/answer' //removeadmin
    }, //removeadmin
    {
      path: '',
      redirect: '/main/de'
    },
    {
      path: '/main/:lang',
      component: () => import('#/main/App.vue'),
      redirect: '/main/de/intro',
      props: true,
      title: 'F.A.Q',
      children: [
        { path: 'intro', props: true, component: () => import('#/main/Intro.vue') },
        {
          name: 'map',
          path: 'map',
          props: true,
          meta: { title: 'home' },
          component: () => import('#/main/Map.vue')
        },
        {
          name: 'Area',
          path: 'area/:area/step/:step',
          props(route) {
            const props = { ...route.params }
            props.area = +props.area
            props.step = +props.step
            return props
          },
          component: () => import('#/main/Area.vue')
        },
        { name: 'Faq', path: 'faq', props: true, meta: { title: 'FAQ' }, component: () => import('#/main/Faq.vue') },
        { name: 'Terms', path: 'terms', props: true, meta: { title: 'Terms'}, component: () => import('#/main/Terms.vue') },
        {
          name: 'Sent',
          path: 'sent',
          props: true,
          meta: { title: 'sent' },
          component: () => import('#/main/Sent.vue')
        },
      ]
    },
    {
      name: 'Error',
      path: '/error',
      component: () => import('#/components/Error.vue'),
      props: true
    },
    { path: '', redirect: '/admin/answer' }, //removeadmin
    { //removeadmin
      path: '/admin', //removeadmin
      redirect: '/admin/answer', //removeadmin
      meta: { title: 'F.A.Q - Admin' } //removeadmin
    }, //removeadmin
    { //removeadmin
      path: '/admin/:type', //removeadmin
      component: () => import('#/admin/App.vue'), //removeadmin
      props: true, //removeadmin
      meta: { title: 'F.A.Q - Admin' } //removeadmin
    } //removeadmin
  ],
  scrollBehavior (to, from, savedPosition) {
    return { left: 0, top: 0 }
  }
})


// for each route
// 1) Check if we have ID
//    without ID => Generate ID => go to /
//    with ID => continue to URL
// 2) Check if we have the questions
//    with Questions => continue
//    without Questions => fetch Questions => go to /
router.beforeEach(async (to, from) => {
  //console.log(from.path, to.path)

  if (to.path.includes('admin')) return true
  
  // if (!store.id) {
  //  mutations.setRandomId()
  //   return '/'
  // }
  
  if (!store.questions) {
    // console.log('fetchQuestions 1')
    await fetchQuestions()
    // console.log('fetchQuestions 3')
    return '/'
  } else {
    return true
  }
})

router.afterEach((to, from) => {
  // console.log('after')
  document.title = to.meta.title ? 'F.A.Q - ' + to.meta.title : 'F.A.Q'
})


